(ns moon.core)

(enable-console-print!)

(defn update-world [w]
  (-> w
      (update-in [:fuel] dec)
      (update-in [:x] dec)
      (update-in [:y] dec)))
