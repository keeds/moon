(ns moon.core-test
  (:require
   [cljs.test :refer-macros [deftest testing is]]
   [moon.core :as m]))

(deftest base
  (testing "base"
    (let [w {:x 100 :y 100 :fuel 100}
          w1 (m/update-world w)
          w2 (m/update-world w1)
          w3 (m/update-world w2)
          w' (loop [n w
                    x 5]
               (if (< x 1)
                 n
                 (recur (m/update-world n) (dec x))))]
      (is (= 99 (:fuel w1)))
      (is (= 95 (:fuel w'))))))
